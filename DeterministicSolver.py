from pulp import *
from Solution import Solution


class DeterministicSolver( object ):
    def __init__(self, instance):
        self.Model = LpProblem("The_Deterministic_Pushasing_Plan", LpMinimize)
        self.Instance = instance
        self.DeterminiticDelta = []
        self.GenerateDeterministicDelta()
        #self.BigM = sum(self.Instance.Demand[t] for t in self.Instance.SetTimePeriod)

    def CreateModel(self):
        self.CreateVaribales()
        self.CreateObjectif()
        self.CreateInventoryConstraints()



    def CreateVaribales(self):

        self.Qtc = pulp.LpVariable.dicts('Qtc',
                                         ((t, c) for t in self.Instance.SetTimePeriod
                                          for c in self.Instance.SetComponents),
                                         lowBound=0,
                                         cat=pulp.LpContinuous)

        self.Itc = pulp.LpVariable.dicts('Itc',
                                         ((t, c) for t in self.Instance.SetTimePeriod
                                          for c in self.Instance.SetComponents),
                                         lowBound=0,
                                         cat=pulp.LpContinuous)


    def CreateObjectif(self):
        self.Model += (
            pulp.lpSum([
                        self.Instance.HoldingCost[c] * self.Itc[(t, c)]
                        for t in self.Instance.SetTimePeriod for c in self.Instance.SetComponents
                       ])
            )

    def CreateInventoryConstraints(self):

        for t in self.Instance.SetTimePeriod:
            for c in self.Instance.SetComponents:
                cummulativedemand = sum(self.Instance.Requirements[p][c]
                                        * self.Instance.Demand[p][tau]
                                         for tau in range(t+1)
                                         for p in self.Instance.SetEndItems)


                condition =  self.Itc[(t, c)]  ==  self.Instance.InitialInventory[c] \
                              + (pulp.lpSum(self.DeterminiticDelta[tau][t][c] * self.Qtc[(tau, c)]
                                                     for tau in range(t+1)
                                                      ) \
                              - cummulativedemand)
                self.Model += condition



    def WriteLP(self):
        self.Model.writeLP("DeterministicModel.lp")

    def SolveLP(self):
        self.Model.solve(CPLEX(msg=1))


    def GetSolution(self):
        sol = Solution()
        sol.TotalCost = value(self.Model.objective)
        sol.orderQuantity = [[self.Qtc[(t,c)].varValue
                              for t in self.Instance.SetTimePeriod]
                             for c in self.Instance.SetComponents]
        sol.InventoryLevel = [[self.Itc[(t, c)].varValue
                               for t in self.Instance.SetTimePeriod]
                              for c in  self.Instance.SetComponents]

        return sol

    def GenerateDeterministicDelta(self):
            self.DeterminiticDelta = [[[1 if t >= tau + self.Instance.AvgLeadTime[c]
                                        else 0
                                        for c in self.Instance.SetComponents]
                                       for t in self.Instance.SetTimePeriod]
                                      for tau in self.Instance.SetTimePeriod]
