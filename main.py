from Instance import  Instance
from InstanceReader import  InstanceReader
from DeterministicSolver import  DeterministicSolver
from StochasticSolver import  StochasticSolver


def Simulate(qty):
    simulator = StochasticSolver(instance)
    simulator.Simulator(qty)
    simulator.SolveLP()
    simulatedsolution = simulator.GetSolution()
    if not simulatedsolution is None:
      simulatedsolution.Print()

if __name__ == '__main__':
    print("------------Hello----------------")


    reader = InstanceReader("./Data/DATA_ATO.xlsx")
    reader.Read()
    instance = reader.Instance

    solver = DeterministicSolver(instance)
    solver.CreateModel()
    #solver.WriteLP()
    solver.SolveLP()

    solution = solver.GetSolution()
    solution.Print()
    Simulate(solution.orderQuantity)

    for sampling in ["CMC", "LHS"]:
        for n in [2, 5, 10, 50, 100]:
            print("*****************" )
            print("n %r sampl:%r" %(n, sampling))
            print("****In sample****")
            solver = StochasticSolver(instance)
            solver.SampleStratey = sampling
            solver.CreateModel(n)
            #solver.WriteLP()
            solver.SolveLP()

            solution = solver.GetSolution()
            solution.Print()
            print("****Out of sample****")
            Simulate(solution.orderQuantity)

