from pulp import *
from numpy import  *
from Solution import Solution
from pyDOE import *

class StochasticSolver( object ):
    def __init__(self, instance):
        self.Model = LpProblem("The_stochastic_pushasing_plan", LpMinimize)
        self.Instance = instance
        self.SetScenario = []
        self.ProbaScenario = []
        self.StochasticDelta = []
        self.SampleStratey = "LHS"
        self.simulation = False
        #self.BigM = sum(self.Instance.Demand[t] for t in self.Instance.SetTimePeriod)

    def CreateModel(self, n):
        self.GenerateScenario(n)
        self.CreateVaribales()
        self.CreateObjectif()
        self.CreateInventoryConstraints()
        self.CreateDemandConstraints()
        self.CreateServiceLevelConstraints()
        self.CreateATOConstraints()

    def CreateVaribales(self):

        self.Qtc = pulp.LpVariable.dicts('Qtc',
                                         ((t, c) for t in self.Instance.SetTimePeriod
                                          for c in self.Instance.SetComponents),
                                         lowBound=0,
                                         cat=pulp.LpContinuous)

        self.Itcw = pulp.LpVariable.dicts('Itcw',
                                         ((t, c, w) for t in self.Instance.SetTimePeriod
                                          for c in self.Instance.SetComponents
                                          for w in self.SetScenario),
                                         lowBound=0,
                                         cat=pulp.LpContinuous)

        self.Xitw = pulp.LpVariable.dicts('Xtiw',
                                         ((t, i, w) for t in self.Instance.SetTimePeriod
                                          for i in self.Instance.SetEndItems
                                          for w in self.SetScenario),
                                         lowBound=0,
                                         cat=pulp.LpContinuous)

        self.aw = pulp.LpVariable.dicts('aw',
                                          (( w)
                                           for w in self.SetScenario),
                                          lowBound=0,
                                          cat=pulp.LpBinary)
        if self.simulation:
            self.sl = pulp.LpVariable('sl',
                                              lowBound=0,
                                              cat=pulp.LpContinuous)

    def CreateObjectif(self):
        if self.simulation:
            self.Model += 10000000*self.sl + (
                pulp.lpSum([
                    self.ProbaScenario[w]
                    * self.Instance.HoldingCost[c]
                    * self.Itcw[(t, c, w)]
                    for t in self.Instance.SetTimePeriod
                    for c in self.Instance.SetComponents
                    for w in self.SetScenario
                ]))
        else:

            self.Model += (
                pulp.lpSum([
                            self.ProbaScenario[w]
                            * self.Instance.HoldingCost[c]
                            * self.Itcw[(t, c, w)]
                            for t in self.Instance.SetTimePeriod
                            for c in self.Instance.SetComponents
                            for w in self.SetScenario
                           ])
                )



    def CreateDemandConstraints(self):

        for t in self.Instance.SetTimePeriod:
            for i in self.Instance.SetEndItems:
                for w in self.SetScenario:

                    condition =  self.Instance.Demand[i][t] - self.Xitw[(t, i, w)]  <= \
                                 self.Instance.Demand[i][t] * self.aw[( w)]

                    self.Model += condition

    def CreateInventoryConstraints(self):

          for t in self.Instance.SetTimePeriod:
                for c in self.Instance.SetComponents:
                     for w in self.SetScenario:
                          condition = self.Itcw[(t, c, w)] == \
                                      self.Instance.InitialInventory[c] \
                                      + (pulp.lpSum(self.StochasticDelta[w][tau][t][c]
                                                   * self.Qtc[(tau, c)]
                                                    for tau in range(t + 1))) \
                                      - (pulp.lpSum(self.Instance.Requirements[i][c]
                                                    * self.Xitw[(tau, i, w)]
                                                    for tau in range(t + 1)
                                                    for i in self.Instance.SetEndItems))

                          self.Model += condition

    def CreateServiceLevelConstraints(self):

                    if self.simulation:
                        condition = (pulp.lpSum(self.ProbaScenario[w]
                                                    * self.aw[(w)]
                                                    for w in self.SetScenario)) \
                                        <= 0.1 +  self.sl
                    else:
                        condition = (pulp.lpSum(self.ProbaScenario[w]
                                                * self.aw[( w)]

                                                for w in self.SetScenario)) \
                                    <= 0.1
                    self.Model += condition

    def CreateATOConstraints(self):
        for w in self.SetScenario:
            for t in self.Instance.SetTimePeriod:
                for i in self.Instance.SetEndItems:
                    condition = self.Xitw[(t, i, w)] \
                                <= self.Instance.Demand[i][t]

                    self.Model += condition

    def WriteLP(self):
        self.Model.writeLP("StochasticModel.lp")

    def SolveLP(self):
        self.Model.solve(CPLEX(msg=0))
        print("Status:", LpStatus[self.Model.status])


    def GetSolution(self):
        if  not LpStatus[self.Model.status] == "Infeasible":
            sol = Solution()
            sol.TotalCost = value(self.Model.objective)
            sol.InventoryCost = sum(self.ProbaScenario[w] \
                                   * self.Instance.HoldingCost[c] \
                                   * self.Itcw[(t, c, w)].varValue
                                    for t in self.Instance.SetTimePeriod
                                   for c in self.Instance.SetComponents
                                  for w in self.SetScenario)

            sol.orderQuantity = [[self.Qtc[(t,c)].varValue
                                  for t in self.Instance.SetTimePeriod]
                                 for c in self.Instance.SetComponents]
            sol.InventoryLevel = [[[self.Itcw[(t, c, w)].varValue
                                   for t in self.Instance.SetTimePeriod]
                                  for c in  self.Instance.SetComponents]
                                 for w in self.SetScenario]
            sol.Production = [[[self.Xitw[(t, i, w)].varValue
                                    for t in self.Instance.SetTimePeriod]
                                   for i in self.Instance.SetEndItems]
                                  for w in self.SetScenario]

            sol.service =  [self.aw[(w)].varValue
                                  for w in self.SetScenario]

            sol.AvgService =  sum(self.ProbaScenario[w] *self.aw[( w)].varValue
                                  for w in self.SetScenario
                                  )

            return sol
        else:
            return None


    def GenerateScenario(self, n):
        self.SetScenario = [ w for w in range(n)]
        self.ProbaScenario=[1.0 / n for w in range(n)]
        self.StochasticDelta = [[[[0
                                    for c in self.Instance.SetComponents]
                                   for t in self.Instance.SetTimePeriod]
                                  for tau in self.Instance.SetTimePeriod]
                                  for w in self.SetScenario]




        L01 = self.Sample(n)
        for w in range(n):
            L =  [ [ 0
                     for t in self.Instance.SetTimePeriod]
                 for c in self.Instance.SetComponents]

            for c in self.Instance.SetComponents:
                 for t in self.Instance.SetTimePeriod:
                     k=-1
                     cum=0
                     while cum < L01[w][c][t] :
                         k=k+1
                         cum += self.Instance.LeadTimeDistribution[c][k]
                     L[c][t] = k
            #print("Lead time scenario %r:%r" % (w, L))

            self.StochasticDelta[w] = [[[1 if t >= tau + L[c][tau]
                                       else 0
                                       for c in self.Instance.SetComponents]
                                      for t in self.Instance.SetTimePeriod]
                                     for tau in self.Instance.SetTimePeriod]


    def Sample(self,n):
        L01 = None
        if(self.SampleStratey == "CMC"):
            L01 = [[[random.uniform(0, 1)
                     for t in self.Instance.SetTimePeriod]
                    for c in self.Instance.SetComponents]
                    for w in self.SetScenario]

        if (self.SampleStratey == "LHS"):
            Lv01 = lhs(self.Instance.NrComponents*self.Instance.NrTimePeriods,
                       samples=n)
            L01 = [[[Lv01[w][c*self.Instance.NrTimePeriods+t]
                     for t in self.Instance.SetTimePeriod]
                     for c in self.Instance.SetComponents]
                     for w in self.SetScenario]
        return L01


    def Simulator(self, PlannedQ):
        self.simulation = True
        self.SampleStratey = "CMC"
        self.GenerateScenario(200)
        self.CreateVaribales()
        self.CreateObjectif()
        self.CreateInventoryConstraints()
        self.CreateDemandConstraints()
        self.CreateServiceLevelConstraints()
        self.CreateATOConstraints()
        for t in self.Instance.SetTimePeriod:
                for c in self.Instance.SetComponents:
                    condition = self.Qtc[(t, c)] \
                                == PlannedQ[c][t]

                    self.Model += condition
