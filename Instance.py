

class Instance( object ):
    def __init__(self):
        self.NrTimePeriods = 7
        self.SetTimePeriod = [i for i in range(self.NrTimePeriods)]
        self.NrComponents = -1
        self.NrEndItem = -1

        self.SetComponents = []
        self.SetEndItems = []
        self.Requirements = []
        self.Demand = []
        self.LeadTimeDistribution = []
        self.MaxLeadTime = 5
        self.HoldingCost = -1
        self.InitialInventory = [0] * self.NrComponents
        self.AvgLeadTime = []


    def Print(self):
        print("nr period: %r"%self.NrTimePeriods)
        print("nr components:%r"%self.NrComponents)
        print("nr end items: %r"%self.NrEndItem)

        print("set time periods: %r"%self.SetTimePeriod)
        print("set components: %r"%self.SetComponents)
        print("set end-items: %r"%self.SetEndItems)

        print("Requirement: %r"%self.Requirements)
        print("demand: %r"%self.Demand)
        print("initial inv: %r" % self.InitialInventory)
        print("holding costs: %r" % self.HoldingCost)
        print("lead time distribution %r"%self.LeadTimeDistribution)
        print("average lead time %r" % self.AvgLeadTime)




