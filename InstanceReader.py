from Instance import  Instance
from random import randint
import math
import numpy as np
import openpyxl as opxl
import itertools as itools
import pandas as pd
from Tool import Tool

import math
import scipy as scipy

class InstanceReader():

    # Constructor
    def __init__(self, filename=""):
        self.Instance = Instance()
        self.HoldingCostdf = None
        self.LeadTimesDistirbutiondf = None
        self.BOMdf = None
        self.Demanddf = None
        self.Filename = filename
        # This funciton read the instance from the file ./Instances/MSOM-06-038-R2.xlsx

    def Read(self):
        self.OpenFiles(self.Filename)
        self.CreateProductList()
        self.CreateRequirement()
        self.CreateLeadTimeDistribution()
        self.CreateHoldingCostCost()
        self.CreateDemand()

    # Create datasets from the sheets for instance from Grave 2008
    def OpenFiles(self, instancename):
        wb2 = opxl.load_workbook(self.Filename)
        # The supplychain is defined in the sheet named "01_LL" and the data are in the sheet "01_SD"
        self.HoldingCostdf = Tool.ReadDataFrame(wb2, "Holding costs")
        self.LeadTimesDistirbutiondf = Tool.ReadDataFrame(wb2, "Lead Times Distirbution")
        self.BOMdf = Tool.ReadDataFrame(wb2, "BOM")
        self.Demanddf = Tool.ReadDataFrame(wb2, "Demand")

    def CreateProductList(self):
        self.Instance.ProductName = [] #self.Datasheetdf.Index#[row[0] for row in self.DTFile]
        self.Instance.ComponentName = []  # self.Datasheetdf.Index#[row[0] for row in self.DTFile]

        for  row in self.BOMdf.index:
            self.Instance.ComponentName.append(row)

        for column in self.BOMdf.columns:
            self.Instance.ProductName.append(column)

        self.Instance.NrComponents=len(self.Instance.ComponentName)
        self.Instance.NrEndItem = len(self.Instance.ProductName)
        self.Instance.SetComponents = [i for i in range(self.Instance.NrComponents)]
        self.Instance.SetEndItems = [i for i in  range(self.Instance.NrEndItem)]
        self.Instance.InitialInventory = [0] * self.Instance.NrComponents

    def CreateRequirement(self):
        self.Instance.Requirements = [[0] * self.Instance.NrComponents for _ in self.Instance.SetEndItems]
        for p in self.Instance.SetEndItems:
            for c in self.Instance.SetComponents:
                self.Instance.Requirements[p][c] =self.BOMdf.at[self.Instance.ComponentName[c], self.Instance.ProductName[p]]



    # This function creates the lead times
    def CreateLeadTimeDistribution(self):
        self.Instance.LeadTimeDistribution = [ [0 for l in range(self.Instance.MaxLeadTime+1)] for p in self.Instance.SetComponents]
        for c in self.Instance.SetComponents:
            for l in range(1,self.Instance.MaxLeadTime+1):
                self.Instance.LeadTimeDistribution[c][l] = self.LeadTimesDistirbutiondf.at[self.Instance.ComponentName[c],l]

        self.Instance.AvgLeadTime = [ math.ceil(
                                                sum( (l) * self.Instance.LeadTimeDistribution[c][l]
                                                     for l in range(1,self.Instance.MaxLeadTime+1))
                                                )
                                      for c in self.Instance.SetComponents]



    #Generate the inventory costs
    def CreateHoldingCostCost(self):

        self.Instance.HoldingCost = [ 0 for p in self.Instance.SetComponents]

        for c in self.Instance.SetComponents:
               self.Instance.HoldingCost[c] = self.HoldingCostdf.at[self.Instance.ComponentName[c],'holding costs']


    # Generate the Demand
    def CreateDemand(self):

        self.Instance.Demand = [[0 for t in self.Instance.SetTimePeriod]for p in self.Instance.SetEndItems]

        for p in self.Instance.SetEndItems:
            for t in self.Instance.SetTimePeriod:
                self.Instance.Demand[p][t] = self.Demanddf.at[self.Instance.ProductName[p],t+1]

