import pandas as pd
import csv
from datetime import datetime
import math
from Instance import Instance
from Tool import Tool
import openpyxl as opxl
from ast import literal_eval
import numpy as np
#from matplotlib import pyplot as plt

class Solution:
    # constructor
    def __init__(self, instance=None, solquantity=None, solproduction=None, solinventory=None,
                 solbackorder=None,solservice=None):
        self.Instance = instance
        self.orderQuantity = solquantity
        self.InventoryLevel = solinventory
        self.Production = solproduction
        self.BackOrder = solbackorder
        self.service = solservice
        self.InventoryCost = -1
        self.TotalCost = -1
        self.AvgService  = -1
        self.MaxService = -1
        self.InventoryCost = -1

    def Print(self):
        #print("order quantity %r"%self.orderQuantity)
        #print("inventory level %r" % self.InventoryLevel)
        #print("Production %r" % self.Production)
        #print("Servise %r" % self.service)
        print("AvgServise %r" % self.AvgService)
        print("TotalCost %r" % self.TotalCost)
        #print("MaxService %r" % self.MaxService)
        print("InventoryCost %r" % self.InventoryCost)





    # This function print the solution different pickle files

    #This function print the solution in an Excel file in the folde "Solutions"
    def PrintToExcel(self, description):
        prodquantitydf, inventorydf, productiondf, bbackorderdf, svaluedf, fixedqvaluesdf = self.DataFrameFromList()
        writer = pd.ExcelWriter( self.GetSolutionFileName( description ), engine='openpyxl')
        #givenquantty = [[self.ProductionQuantity.ix[p, t].get_value(0) for p in self.MRPInstance.ProductSet]
        #                for t in self.MRPInstance.TimeBucketSet]
        #toprint = pd.DataFrame( givenquantty )

        prodquantitydf.to_excel(writer, 'ProductionQuantity')
        inventorydf.to_excel(writer, 'InventoryLevel')
        bbackorderdf.to_excel(writer, 'BackOrder')


        writer.save()

    def ReadExcelFiles(self, description, index = "", indexbackorder = ""):
        # The supplychain is defined in the sheet named "01_LL" and the data are in the sheet "01_SD"
        prodquantitydf = Tool.ReadMultiIndexDataFrame(self.GetSolutionFileName(description), "ProductionQuantity" )
        return prodquantitydf


